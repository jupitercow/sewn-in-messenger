jQuery(document).ready(function($) {

	var overlay_bg_opacity = '0.7',
		to_id              = false,
		reply_id           = false,
		$filter_add_submit = $('form.acf_messenger_add_filter input[type="submit"]'),
		$filter_add_text   = $('form.acf_messenger_add_filter input[type="text"]');

		$filter_add_submit.hide();
		$filter_add_text.on('propertychange keyup input paste', function(){
			if ( $filter_add_text.val() )
			{
				$filter_add_submit.show();
			}
			else
			{
				$filter_add_submit.hide();
			}
		});

	/**
	 * Use to mark a message as read, archive, new, etc
	 */
	$(document).on('acf_messenger/mark_message', function(e, args) {

    	var defaults = {
    			message_id : 0,
    			term       : '',
    			nonce      : '',
    			success    : function( response ) {},
    			error      : function( xhr ) { console.log(xhr.responseText); },
    		},
    		args = $.extend(defaults, args);

		// Mark the message as read
    	if ( args.message_id )
	    {
			$.ajax({
				url:      messenger.url,
				type:     'post',
				async:    true,
				cache:    false,
				dataType: 'json',
				data: {
					'action' :     messenger.actions.mark,
					'message_id' : args.message_id,
					'term' :       args.term,
					'nonce' :      args.nonce,
				},
				success: args.success,
				error: args.error,
			});
		}

	});

	/**
	 * Creates a bootstrp modal
	 */
	$(document).on('acf_messenger/modal', function(e, args) {
    	var defaults = {
    			remote: '',
    			title: '',
    		},
    		args = $.extend(defaults, args);

		if ( $('#message-modal').length )
		{
			$('#message-modal').modal('hide');
		}

		var $modal               = $('<div />', {'id': "message-modal", 'class': "modal fade", 'tabindex': "-1", 'aria-labelledby': "message-modal-label", 'aria-hidden': "true", 'role': "dialog"}),
			$modal_dialog        = $('<div />', {'class': "modal-dialog"}).appendTo($modal),
			$modal_content       = $('<div />', {'class': "modal-content"}).appendTo($modal_dialog),
			$modal_header        = $('<div />', {'class': "modal-header"}),
			$modal_body          = $('<div />', {'class': "modal-body"}),
			$modal_footer        = $('<div />', {'class': "modal-footer"}),
			$modal_header_close  = $('<button />', {'class': "close", 'data-dismiss': "modal", 'aria-hidden': "true", 'type': "button", 'html': "&times;"}).appendTo($modal_header),
			$modal_header_title  = $('<h4 />', {'class': "modal-title", 'text': args.title,  'id': "message-modal-label"}),
			$modal_footer_close  = $('<button />', {'class': "btn btn-default", 'data-dismiss': "modal", 'text': "Close"}).appendTo($modal_footer);

		$modal_header_title.appendTo($modal_header);

		$modal
			.on('show.bs.modal', function () {
				// Make sure the body has the modal-open tag
				$('body').addClass('modal-open');
			})
			.on('hidden.bs.modal', function () {
				// Make sure the body has the modal-open tag
				$('body').removeClass('modal-open');
				// Remove the modal when it is hidden, start over each time
				$(this).remove();
			})
			.on('loaded.bs.modal', function () {
				// If h1 in the loaded content, use it as a title, and remove it
				var $content_h1 = $('h1', $modal_content),
					title = $content_h1.html();

				if ( title && title.length )
				{
					$content_h1.remove();
					$modal_header_title.html( title );
				}

				// Wrap the new content in a modal-body class and add the header
				$modal_content.wrapInner($modal_body).prepend($modal_header);//.append($modal_footer);
			})
			.modal({
				remote: args.remote + ' ' + messenger.view_selector,
				show: true
			});
	});

	/**
	 * Open colorbox
	 */
	$(document).on('acf_messenger/colorbox', function(e, args) {
    	var defaults = {
    			remote : '',
    		},
    		args = $.extend(defaults, args);

		$.colorbox({trapFocus: false, href: args.remote + ' ' + messenger.view_selector, width:"80%", maxWidth:"1024px", opacity: '0.7'});
	});

	/**
	 * Open an overlay
	 */
	$(document).on('acf_messenger/overlay', function(e, args) {
    	var defaults = {
    			remote : '',
    		},
    		args = $.extend(defaults, args);

    	// Open the overlay
		if ( 'undefined' !== typeof($.fn.modal) )
		{
			$(document).trigger('acf_messenger/modal', args);
		}
		else
		{
			$(document).trigger('acf_messenger/colorbox', args);
		}
	});

	/**
	 * Add select2 to the user field
	 * /
	//$(document).on('click''#acf-field-to').select2({width:'element'});//{ 'dropdownAutoWidth' : true }
	$(document).on('acf/setup_fields cbox_complete shown.bs.modal', function(e, div) {
		$(document).trigger( 'acf_messenger/add_select2' );
	});
	$(document).on('acf_messenger/add_select2', function(e, args) {
		$("#acf-field-to").addClass('form-control').select2({width: 'element'});//{dropdownAutoWidth: true});
	});

	/**
	 * Update message form with reply and/or to:user info
	 */
	$(document).on('click', '.message-compose', function(e) {
		e.preventDefault();

		var $this = $(this);

    	// Open the overlay
    	$(document).trigger( 'acf_messenger/overlay', {remote: $this.attr('href'), title: "Compose Message"} );
	});

    /**
     * Read message popup
     */
	$(document).on('click', '.read-message', function(e) {
    	e.preventDefault();

    	var $this   = $(this),
    		$target = $this.closest('.message'),
    		args    = {
	    		message_id : $target.data('id'),
				nonce      : $this.data('nonce'),
    			term       : 'read',
    			success    : function( response ) {
	    			//console.log(response);
					if ( response.status )
					{
						$('.new-indicator', $target).remove();
						$target.removeClass('unread').addClass('read');
						// Open the overlay
						$(document).trigger( 'acf_messenger/overlay', {remote: $this.attr('href'), title: "Message"} );
					}
    			},
    		};

		$(document).trigger( 'acf_messenger/mark_message', args );
    });

});