jQuery(document).ready(function($) {

	var overlay_bg_opacity = '0.7',
		to_id = false,
		reply_id = false;

	/**
	 * Use to mark a message as read, archive, new, etc
	 */
	$(document).on('acf_messenger/mark_message', function(e, args) {

    	var defaults = {
    			message_id : 0,
    			term       : '',
    			nonce      : '',
    			success    : function( response ) {},
    			error      : function( xhr ) { console.log(xhr.responseText); },
    		},
    		args = $.extend(defaults, args);

		// Mark the message as read
    	if ( args.message_id )
	    {
			$.ajax({
				url:      messenger.url,
				type:     'post',
				async:    true,
				cache:    false,
				dataType: 'json',
				data: {
					'action' :     messenger.actions.mark,
					'message_id' : args.message_id,
					'term' :       args.term,
					'nonce' :      args.nonce,
				},
				success: args.success,
				error: args.error,
			});
		}

	});

	/**
	 * Update message form with reply and/or to:user info
	 * /
	$(document).on('click', '.' + messenger.prefix + '_send-message', function(e) {
		e.preventDefault();

		var $this   = $(this),
			$form   = $this.closest('form'),
			post_id = $form.data('id');

		if ( post_id )
		{
			$form.submit();
		}
		else
		{
			var $this        = $(this),
				$form        = $this.closest('form'),
				subject      = $('#acf-field-form_post_title', $form).val(),
				message      = $('#acf-field-form_post_content', $form).val(),
				$spinner_img = $('<img />', {'src' : messenger.spinner}),
				$spinner     = $('<div />', {'id' : messenger.prefix +'_spinner'}).append( $spinner_img );

				$this.after( $spinner );

			if (! to_id )
			{
				to_id = $('#acf-field-to').val();
			}

			if ( to_id )
			{
				var opts = {
					url:      messenger.url,
					type:     'post',
					async:    true,
					cache:    false,
					dataType: 'json',
					data: {
						'action'  :  messenger.actions.add,
						'to'      :  to_id,
						'subject' :  subject,
						'message' :  message,
						'reply'   :  reply_id,
					},

					success: function( response )
					{
						console.log(response);
						if ( response.status )
						{
							//console.log('true');
							$spinner.remove();
							$('.frontend_notifications').trigger('frontend_notifications/add', [messenger.strings.sent_message, {fade : true}]);//, error : true
							$.colorbox.close();
						}
					},

					error: function( xhr )
					{
						console.log(xhr.responseText);
					}
				};
				$.ajax(opts);
		    }
		}
    });

    /**
     * Archive message
     * /
    $('.archive-message, .unarchive-message').on('click', function(e) {
    	e.preventDefault();

    	var $this   = $(this),
    		$target = $this.parent();

    	var step    = ( $this.hasClass('archive-message') ? 'archive' : 'unarchive' );

    	if ( $this.attr('data-id') )
	    {
			var opts = {
				url:      lelearning_ajax.url,
				type:     'post',
				async:    true,
				cache:    false,
				dataType: 'html',
				data: {
					action:     'lelearning_message_archived',
					message_id: $this.data('id'),
					step:       step
				},

				success: function( response )
				{
					console.log(response);

					if ( response )
					{
						$target.fadeOut();
					}
				},

				error: function( xhr )
				{
					//console.log(xhr.responseText);
				}
			};
			$.ajax(opts);
		}
    });

	/**
	 * Colorbox
	 * /
	$('a.colorbox').colorbox({transition:"fade", maxWidth:"90%", maxHeight:"90%", opacity: '0.7'});
	$('a.popup').colorbox({inline: true, href: $(this).attr('href'), maxWidth:"80%", maxHeight:"90%", opacity: '0.7'});
	/**/

});