<?php
/*
Plugin Name: Sewn In Messenger
Plugin URI: http://bitbucket.org/jupitercow/sewn-in-messenger
Description: Adds an interface for sending and receiving messages between site members. Can be used to comment on posts as well
Version: 1.0.0
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2014 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('sewn_messenger') ) :

add_action( 'init', array('sewn_messenger', 'init') );

class sewn_messenger
{
	/**
	 * Class prefix
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	const PREFIX = __CLASS__;

	/**
	 * Settings
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $settings = array();

	/**
	 * Holds an array of messages retrieved during each load
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $messages;

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function init()
	{
		// vars
		self::$settings = array(
			'path'      => apply_filters( 'acf/helpers/get_path', __FILE__ ),
			'dir'       => apply_filters( 'acf/helpers/get_dir', __FILE__ ),
			'post_type' => 'message',
			'taxonomy'  => 'message_status',
			'pages'     => array(
				'message-compose' => array(
					'page_name'  => 'message-compose',
					'page_title' => 'Compose Message',
				),
				'inbox' => array(
					'page_name'  => 'inbox',
					'page_title' => 'Inbox',
				)
			),
			'strings'   => array(
				'subject_default'    => __("%s sent you a message", 'acf_messenger'),
				'subject_reply'      => __("%s replied to your message", 'acf_messenger'),
				'subject_reply_post' => __("%s left a message on your post", 'acf_messenger'),
			),
			'default_filters' => array(
				'private' => __( "Private", 'acf_messenger' ),
				'public'  => __( "Public", 'acf_messenger' ),
			),
			'messages' => array(
				'message-sent' => array(
					'key'     => 'action',
					'value'   => 'message-sent',
					'message' => __( "Message sent.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
				'message-archive' => array(
					'key'     => 'action',
					'value'   => 'message-archive',
					'message' => __( "Message successfully archived.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
				'message-unarchive' => array(
					'key'     => 'action',
					'value'   => 'message-unarchive',
					'message' => __( "Message successfully unarchived.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
				'message-new' => array(
					'key'     => 'action',
					'value'   => 'message-new',
					'message' => __( "Message successfully updated.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
				'message-new' => array(
					'key'     => 'action',
					'value'   => 'message-read',
					'message' => __( "Message successfully updated.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
				'message-filters' => array(
					'key'     => 'action',
					'value'   => 'message-filters',
					'message' => __( "Filters successfully updated.", 'acf_messenger' ),
					'args'    => 'fade=true',
				),
			),
		);

		self::$settings = apply_filters( self::PREFIX . '/settings', self::$settings );
		self::$settings['strings'] = apply_filters( self::PREFIX . '/strings', self::$settings['strings'] );
		self::$settings['messages'] = apply_filters( self::PREFIX . '/messages', self::$settings['messages'] );

		// set text domain
		#load_textdomain( 'acf_messenger', self::$settings['path'] . 'lang/acf_messenger-' . get_locale() . '.mo' );

		// Set up custom post types
		self::add_post_types();

		// Install options
		self::install();

		// Add custom fields for send form
		self::register_field_group();

		// actions
		add_action( self::PREFIX . '/mark_message/sent',         array(__CLASS__, 'mark_message_sent') );
		add_action( self::PREFIX . '/mark_message/read',         array(__CLASS__, 'mark_message_read') );
		add_action( self::PREFIX . '/mark_message/unread',       array(__CLASS__, 'mark_message_unread') );
		add_action( self::PREFIX . '/mark_message/new',          array(__CLASS__, 'mark_message_unread') );
		add_action( self::PREFIX . '/mark_message/replied',      array(__CLASS__, 'mark_message_replied') );
		add_action( self::PREFIX . '/mark_message/archive',      array(__CLASS__, 'mark_message_archive' ) );
		add_action( self::PREFIX . '/mark_message/archived',     array(__CLASS__, 'mark_message_archive' ) );
		add_action( self::PREFIX . '/mark_message/unarchive',    array(__CLASS__, 'mark_message_unarchive' ) );
		add_action( self::PREFIX . '/mark_message/unarchived',   array(__CLASS__, 'mark_message_unarchive' ) );
		add_action( self::PREFIX . '/mark_message/private',      array(__CLASS__, 'mark_message_private' ) );
		add_action( self::PREFIX . '/mark_message/public',       array(__CLASS__, 'mark_message_public' ) );

		add_action( self::PREFIX . '/new_message_indicator',     array(__CLASS__, 'new_message_indicator') );
		add_action( self::PREFIX . '/add_message',               array(__CLASS__, 'add_message') );
		add_action( self::PREFIX . '/send_mail',                 array(__CLASS__, 'send_mail') );

		add_action( self::PREFIX . '/inbox',                     array(__CLASS__, 'inbox') );
		add_action( self::PREFIX . '/comments',                  array(__CLASS__, 'comments') );
		add_action( self::PREFIX . '/comments/public',           array(__CLASS__, 'comments_public') );
		add_action( self::PREFIX . '/comments/private',          array(__CLASS__, 'comments_private') );
		add_action( self::PREFIX . '/message_list',              array(__CLASS__, 'message_list'), 10, 2 );
		add_action( self::PREFIX . '/message/user',              array(__CLASS__, 'message_user') );
		add_action( self::PREFIX . '/message/post',              array(__CLASS__, 'message_post') );
		add_action( self::PREFIX . '/permalink',                 array(__CLASS__, 'permalink') );

		add_action( self::PREFIX . '/compose_form',              array(__CLASS__, 'compose_form') );
		add_action( self::PREFIX . '/compose_button',            array(__CLASS__, 'compose_button') );
		add_action( self::PREFIX . '/archive_button',            array(__CLASS__, 'archive_button') );
		add_action( self::PREFIX . '/read_button',               array(__CLASS__, 'read_button') );
		add_action( self::PREFIX . '/view_archive_button',       array(__CLASS__, 'view_archive_button') );
		add_action( self::PREFIX . '/form_head',                 array(__CLASS__, 'form_head') );
		add_action( self::PREFIX . '/enqueue_scripts',           array(__CLASS__, 'enqueue_scripts') );

		// 
		add_action( self::PREFIX . '/filters/choose',            array(__CLASS__, 'filters_choose') );
		add_action( self::PREFIX . '/filters/list',              array(__CLASS__, 'filters_list') );
		add_action( self::PREFIX . '/filters/add_new',           array(__CLASS__, 'filters_add_new') );
		add_filter( 'acf/load_field/name=message_filters',        array(__CLASS__, 'get_message_filters') );

		// ajax actions
		add_action( 'wp_ajax_' . self::PREFIX . '_mark_message', array(__CLASS__, 'ajax_mark_message') );
		add_action( 'wp_ajax_' . self::PREFIX . '_add_message',  array(__CLASS__, 'ajax_add_message') );

		// filters
		add_filter( self::PREFIX . '/get_messages',              array(__CLASS__, 'get_messages') );
		add_filter( self::PREFIX . '/get_user_messages',         array(__CLASS__, 'get_user_messages') );
		add_filter( self::PREFIX . '/get_post_messages',         array(__CLASS__, 'get_post_messages') );
		add_filter( self::PREFIX . '/total_messages',            array(__CLASS__, 'total_messages') );
		add_filter( self::PREFIX . '/get_template',              array(__CLASS__, 'get_template') );
		add_filter( self::PREFIX . '/get_permalink',             array(__CLASS__, 'get_permalink') );
		add_filter( self::PREFIX . '/inbox_url',                 array(__CLASS__, 'inbox_url'), 0 );

		// external customization
		add_filter( 'acf/load_field',                             array(__CLASS__, 'customize_edit_title_content'), 10, 2 );
		add_filter( 'template_include',                           array(__CLASS__, 'template_include'), 99 );
		add_filter( 'the_posts',                                  array(__CLASS__, 'add_post'), 1, 2 );
		add_filter( 'acf/pre_save_post',                          array(__CLASS__, 'acf_pre_save_post'), 0 );
		add_action( 'pre_get_posts',                              array(__CLASS__, 'pre_get_posts') );

		// Add some actions to page load
		add_action( 'wp',                                         array(__CLASS__, 'mark_message_url') );

		add_filter( 'customize_subscription/types',               array(__CLASS__, 'subscription_type') );
	}

	/**
	 * Basic interface and setup for inbox url
	 *
	 * apply_filters( self::PREFIX . '/inbox_url', '' )
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function inbox_url( $url )
	{
		return home_url( '/' . self::$settings['pages']['inbox']['page_name'] . '/' );
	}

	/**
	 * Add the select to choose filters
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function filters_choose( $args )
	{
		$defaults = array(
			'return_url'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		if ( 'inbox' == $args['return_url'] ) $args['return_url'] = apply_filters( self::PREFIX . '/inbox_url', '' );
		extract( $args, EXTR_SKIP );

		$field_groups = array( 'acf_messenger_filters' );
		?>
		<form id="post" class="<?php echo self::PREFIX; ?>_filters" action="" method="post">
			<?php
			// ACF Form
			acf_form( array(
				'form'         => false,
				'field_groups' => $field_groups,
				'return'       => add_query_arg( 'action', 'message-filters', $return_url ),
			) ); ?> 
			<div class="field">
				<input class="btn btn-default btn-xs" type="submit" value="<?php echo apply_filters( self::PREFIX . '/button/update', __( "Update Filters", 'acf_messenger' ) ); ?>" />
			</div>
		</form>
		<?php /**/
	}

		public static function get_message_filters( $field )
		{
			$special_filters = array('Public', 'Private');
			$filters = get_user_meta( get_current_user_id(), 'message_filters', true );

			if ( is_array($filters) ) 
			{
				// reset choices
				$field['choices'] = array();
				foreach ( $filters as $filter )
				{
					if (! in_array($filter, $special_filters) ) $field['choices'][$filter] = $filter;
				}
			}
			return $field;
		}

	/**
	 * Add a new filter
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function filters_add_new()
	{
		$field_groups = array( 'acf_messenger_add_filter' );
		?>
		<form id="post" class="<?php echo self::PREFIX; ?>_add_filter col-md-12 clearfix" action="" method="post">
			<?php
			// ACF Form
			$args = array(
				'post_id'      => self::PREFIX . '_new_filter',
				'form'         => false,
				'field_groups' => $field_groups,
			);
			acf_form( $args ); ?> 
			<div class="field">
				<input class="btn btn-default btn-xs" type="submit" value="<?php echo apply_filters( self::PREFIX . '/button/add', __( "Add", 'acf_messenger' ) ); ?>" />
			</div>
		</form>
		<?php /**/
	}

	/**
	 * Create a list of filter buttons to load a filtered list in the inbox
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function filters_list()
	{
		$default_filters = apply_filters( self::PREFIX . '/default_filters', self::$settings['default_filters'] );
		$filters = get_user_meta( get_current_user_id(), 'message_filters', true );
		if (! is_array($filters) ) $filters = array();
		$filters = array_merge( $default_filters, $filters );
		$filters = array_unique($filters);

		$url = apply_filters( self::PREFIX . '/inbox_url', get_permalink() );

		$current_filters = $new_filters = array();
		if (! empty($_REQUEST['filter']) )
		{
			$current_filters = explode(',', $_REQUEST['filter']);
			$current_filters = array_unique($current_filters);
		}

		if ( $filters ) 
		{
			// reset choices
			echo '<ul class="' . self::PREFIX . '_filters_list">';
			foreach ( $filters as $filter )
			{
				$new_filters = $current_filters;
				if ( in_array($filter, $current_filters, true) )
				{
					$icon  = 'minus';
					$class = 'primary';
					if ( false !== ($key = array_search($filter, $new_filters)) )
					{
						unset($new_filters[$key]);
					}
				}
				else
				{
					$icon  = 'plus';
					$class = 'default';
					$new_filters[] = $filter;
				}

				if ( 'Public' == $filter && in_array('Private', $current_filters) )
				{
					if ( false !== ($key = array_search('Private', $new_filters)) )
					{
						unset($new_filters[$key]);
					}
				}
				elseif ( 'Private' == $filter && in_array('Public', $current_filters) )
				{
					if ( false !== ($key = array_search('Public', $new_filters)) )
					{
						unset($new_filters[$key]);
					}
				}

				$final_url = ( $new_filters ) ? add_query_arg('filter', implode(',', $new_filters), $url) : $url;
				
				echo '<li><a class="btn btn-' . $class . ' btn-xs" href="' . $final_url . '"><span class="glyphicon glyphicon-'. $icon .'"></span> ' . $filter . '</a></li>';
			}
			echo '</ul>';
		}
	}

	/**
	 * Add messages subscription type if the customize subscription plugin exists
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	array $types The existing types in the customize subscriptions plugin
	 * @return	array $types Modified $posts with the new register post
	 */
	public static function subscription_type( $types )
	{
		$types['messages'] = __("Messages", 'acf_messenger');
		return $types;
	}

	/**
	 * Make updates on page load when the right variables exist
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $posts Modified $posts with the new register post
	 */
	public static function mark_message_url()
	{
		if ( empty($_REQUEST['action']) ) return;
		if ( empty($_REQUEST['nonce']) || ! wp_verify_nonce($_REQUEST['nonce'], $_REQUEST['action']) ) return;
		if ( empty($_REQUEST['id']) ) return;

		do_action( self::PREFIX . '/mark_message/' . $_REQUEST['action'], $_REQUEST['id'] );

		$return_url = ( empty($_REQUEST['return_url']) ) ? apply_filters( self::PREFIX . '/inbox_url', '' ) : $_REQUEST['return_url'];

		wp_redirect( add_query_arg('action', 'message-' . $_REQUEST['action'], $return_url) );
	}

	/**
	 * Add a message on acf form submit
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $posts Modified $posts with the new register post
	 */
	public static function acf_pre_save_post( $post_id )
	{
		// No fields, nothing for us to do
		if ( empty($_POST['fields']) ) return $post_id;

		// check if this is to be a new filter
		if ( self::PREFIX . '_new_filter' == $post_id )
		{
			$default_filters = apply_filters( self::PREFIX . '/default_filters', self::$settings['default_filters'] );
			$filters = get_user_meta( get_current_user_id(), 'message_filters', true );
			if (! is_array($filters) ) $filters = array();
			$filters = array_filter($filters);
			$filters = array_merge( $default_filters, $filters );
			$filters = array_unique($filters);

			$new_filter = $_POST['fields']['field_535113047ef67'];

			$is_new = true;
			foreach ( $filters as $filter )
			{
				if ( strtolower($filter) == strtolower($new_filter) )
					$is_new = false;
			}
			if ( $is_new ) $filters[] = $new_filter;

			update_user_meta( get_current_user_id(), 'message_filters', $filters );

			if (! empty($_POST['return']) )
			{
				wp_redirect( $_POST['return'] );
				die;
			}

			return false;
		}
		// check if this is to be a new message
		elseif ( 'new_message' == $post_id )
		{
			$fields = array();

			/**
			 * Load the fields from $_POST
			 */
			foreach( $_POST['fields'] as $k => $v )
			{
				// get field
				$f = apply_filters( 'acf/load_field', false, $k );
				$f['value'] = $v;
				$fields[$f['name']] = $f;
			}

			if (! empty($_POST['reply']) &&  0 === strpos($_POST['reply'], 'user_') )
			{
				$reply   = false;
				$to      = str_replace('user_', '', $_POST['reply']);
			}
			else
			{
				$reply   = (! empty($_POST['reply']) && is_numeric($_POST['reply']) ) ? $_POST['reply'] : false;
				$to      = ( $reply ) ? get_post_field( 'post_author', $reply ) : false;
			}
			$private = (! empty($_POST['public']) ) ? false : true;

			if (! $to && ! empty($fields['to']) ) $to = $fields['to']['value'];
			#$subject = (! empty($fields['form_post_title']) )   ? $fields['form_post_title']['value']   : '';
			$subject = (! empty($fields['subject']) )           ? $fields['subject']['value']           : '';
			$message = (! empty($fields['form_post_content']) ) ? $fields['form_post_content']['value'] : '';

			$post_id = $_POST['post_id'];
			$args = array(
				'subject'    => ( $subject ? $subject : false ),
				'message'    => $message,
				'to'         => $to,
				'reply'      => $reply,
				'private'    => $private,
			);

			$message_id = self::add_message( $args );

			if (! empty($_POST['return']) )
			{
				wp_redirect( $_POST['return'] );
				die;
			}

			return $message_id;
		}
/** /
echo '<pre style="font-size:0.7em;text-align:left;">';
print_r($_POST);
echo "</pre>\n";
exit;
/**/
		return $post_id;
	}

	/**
	 * See if no compose email page exists, and add it
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $posts Modified $posts with the new register post
	 */
	public static function add_post( $posts, $class )
	{
		global $wp, $wp_query;

		// Check if the requested page matches our target, and no posts have been retrieved
		if (! $posts && array_key_exists(strtolower($wp->request), self::$settings['pages']) && (empty($class->query['post_type']) || 'page' == $class->query['post_type']) )
		{
			// Add the fake post
			$posts   = array();
			$posts[] = self::create_post( strtolower($wp->request) );

			$wp_query->is_page     = true;
			$wp_query->is_singular = true;
			$wp_query->is_home     = false;
			$wp_query->is_archive  = false;
			$wp_query->is_category = false;
			//Longer permalink structures may not match the fake post slug and cause a 404 error so we catch the error here
			unset($wp_query->query["error"]);
			$wp_query->query_vars["error"]="";
			$wp_query->query["post_type"] = 'page';
			$wp_query->query["page"] = $posts[0]->post_name;
			$wp_query->is_404=false;
		}

		return $posts;
	}

	/**
	 * Create a dynamic post on-the-fly for the register page.
	 *
	 * source: http://scott.sherrillmix.com/blog/blogger/creating-a-better-fake-post-with-a-wordpress-plugin/
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $post Dynamically created post
	 */
	public static function create_post( $type )
	{
		// Create a fake post.
		$post = new stdClass();
		$post->ID                    = -1;
		$post->post_author           = 1;
		$post->post_date             = current_time('mysql');
		$post->post_date_gmt         = current_time('mysql', 1);
		$post->post_content          = '';
		$post->post_title            = self::$settings['pages'][$type]['page_title'];
		$post->post_excerpt          = '';
		$post->post_status           = 'publish';
		$post->comment_status        = 'closed';
		$post->ping_status           = 'closed';
		$post->post_password         = '';
		$post->post_name             = self::$settings['pages'][$type]['page_name'];
		$post->to_ping               = '';
		$post->pinged                = '';
		$post->post_modified         = current_time('mysql');
		$post->post_modified_gmt     = current_time('mysql', 1);
		$post->post_content_filtered = '';
		$post->post_parent           = 0;
		$post->guid                  = home_url('/' . self::$settings['pages'][$type]['page_name'] . '/');
		$post->menu_order            = 0;
		$post->post_type             = 'page';
		$post->post_mime_type        = '';
		$post->comment_count         = 0;
		$post->filter                = 'raw';
		return $post;   
	}

	/**
	 * Get plugin templates unless they are overridden in the theme
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $template Template file path
	 * @return	array $template
	 */
	public static function template_include( $template )
	{
		global $wp, $wp_query;

		if ( is_singular( self::$settings['post_type'] ) )
		{
			do_action( 'acf_messenger/form_head' );

			$new_template = locate_template( array( 'single-message.php' ) );
			if ( $new_template )
			{
				return $new_template;
			}
			elseif ( is_file(self::$settings['path'] . 'templates/single-message.php') )
			{
				return self::$settings['path'] . 'templates/single-message.php';
			}
		}
		elseif ( is_page() && array_key_exists(strtolower($wp->request), self::$settings['pages']) )
		{
			do_action( 'acf_messenger/form_head' );

			$new_template = locate_template( array( 'page-' . $wp->request . '.php' ) );
			if ( $new_template )
			{
				return $new_template;
			}
			elseif ( is_file(self::$settings['path'] . 'templates/page-' . $wp->request . '.php') )
			{
				return self::$settings['path'] . 'templates/page-' . $wp->request . '.php';
			}
		}
		return $template;
	}

	/**
	 * Switch out the title with subject on messages
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $template Template file path
	 * @return	array $template
	 */
	public static function pre_get_posts( $query )
	{
		if (! is_admin() && $query->get('post_type') && $query->get('post_type') == self::$settings['post_type'] && $query->is_main_query() )#&& in_the_loop() )
		{
			add_filter( 'the_title', array(__CLASS__, 'subject_as_title'), 999, 2 );
			add_filter( 'wp_title', array(__CLASS__, 'subject_wp_title'), 999, 2 );
		}
	}

	/**
	 * Get subjec to use instead of title
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $template Template file path
	 * @return	array $template
	 */
	public static function subject_wp_title( $title, $sep )
	{
		global $post;
		$subject = get_post_meta( $post->ID, 'subject', true );
        return $subject;
    }

	/**
	 * Get subjec to use instead of title
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $template Template file path
	 * @return	array $template
	 */
	public static function subject_as_title( $title, $post_id )
	{
		if ( get_post_type($post_id) == self::$settings['post_type'] )
		{
			$title = get_post_meta( $post_id, 'subject', true );
		}
        return $title;
    }

	/**
	 * Updates the title and content fields when used for creating emails
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	array $field Field array from ACF
	 * @param	array $key ACF field key
	 * @return	array $field
	 */
	public static function customize_edit_title_content( $field )#, $key )
	{
		global $post;

		if ( is_object($post) && (self::$settings['post_type'] == $post->post_type || 'message-compose' == $post->post_name) )
		{
			if ( 'field_5232d86ba9246title' == $key )
			{
				$field['label'] = __( "Subject", 'acf_messenger' );
			}
			elseif ( 'field_5232d8daa9247content' == $key )
			{
				$field['label'] = __( "Message", 'acf_messenger' );
				$field['type']  = 'textarea';
			}
		}

		return $field;
	}

	/**
	 * Record that message was sent
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_sent( $message_id )
	{
		update_post_meta( $message_id, 'sent', current_time('timestamp') );
	}

	/**
	 * Mark a message as read
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_read( $message_id )
	{
		wp_set_object_terms( $message_id, 'read', self::$settings['taxonomy'], true );
	}

	/**
	 * Mark a message as new
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_unread( $message_id )
	{
		wp_remove_object_terms( $message_id, 'read', self::$settings['taxonomy'] );
	}

	/**
	 * Archive a message
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_archive( $message_id )
	{
		wp_set_object_terms( $message_id, 'archive', self::$settings['taxonomy'], true );
	}

	/**
	 * Unarchive a message
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_unarchive( $message_id )
	{
		wp_remove_object_terms( $message_id, 'archive', self::$settings['taxonomy'] );
	}

	/**
	 * Make a message private
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_private( $message_id )
	{
		wp_set_object_terms( $message_id, 'private', self::$settings['taxonomy'], true );
	}

	/**
	 * Make a message public
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $message_id The post id of the message to be updated
	 * @return	void
	 */
	public static function mark_message_public( $message_id )
	{
		wp_remove_object_terms( $message_id, 'private', self::$settings['taxonomy'] );
	}

	/**
	 * Universal function for updating message terms/meta through ajax
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function ajax_mark_message()
	{
		// defaults for options
		$defaults = array(
			'message_id' =>	0,
			'term'	     =>	'',
			'nonce'      => false,
		);
		$options = array_merge($defaults, $_POST);

		// make sure there is a term to mark
		if (! $options['term'] ) die('bad term');

		// verify nonce
		if (! $options['nonce'] || ! wp_verify_nonce($options['nonce'], $options['term']) ) die('bad nonce');

		// make sure there is an id
		if (! is_numeric($options['message_id']) ) die('bad id');

		do_action( self::PREFIX . '/mark_message/' . $options['term'], $options['message_id'] );

		echo json_encode( array('status'=>1) );
		die();
	}

	/**
	 * Add message to user and/or post.
	 *
	 * @author	Jake Snyder
	 * @since	1.0.0
	 * @param	array $args The arguments
	 *				$subject	string 	(optional) The subject of the message, defaults: "New message from Sitename"
	 *				$message	string 	(required) The message to send
	 *				$to			int|obj (required) ID of user receiving message
	 *				$email		bool 	(optional) Whether or not to email the message as well as add to queue, default: true
	 *				$read		bool 	(optional) Mark message as read? Not sure this should ever be used..., default: false
	 *				$reply		int 	(optional) ID of message that is being replied to, can also be used to attach to a post like a comment, default: false
	 *				$attachment	string 	(optional) Path to file to attach to message, default: false
	 *				$from		int|obj (optional) User ID sending message, default: current user's ID
	 * @return	void
	 */
	public static function add_message( $args )
	{
		$defaults = array(
			'subject'    => false,
			'message'    => false,
			'to'         => false,
			'email'      => true,
			'read'       => false,
			'reply'      => false,
			'comment_id' => false,
			'attachment' => false,
			'from'       => false,
			'private'    => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $message ) return;

		if ( is_numeric($to) )   $to   = get_user_by( 'id', $to );
		if ( is_numeric($from) ) $from = get_user_by( 'id', $from );
		if (! $from )            $from = wp_get_current_user();
		if (! is_object($from) || ! is_object($to) ) return;

		if (! $subject )
		{
			if ( $reply )
			{
				$subject = ( self::$settings['post_type'] == get_post_type($reply) ) ? sprintf(self::$settings['strings']['subject_reply'], $from->display_name) : sprintf(self::$settings['strings']['subject_reply_post'], $from->display_name);
			}
			else
			{
				$subject = sprintf(self::$settings['strings']['subject_default'], $from->display_name);
			}
		}

		// Create a post title to be shown in the admin
		$post_title = $from->ID . "|" . $from->display_name . " > " . $to->ID . "|" . $to->display_name . " on " . current_time('mysql') . ": " . $subject;

		// Store the message
		$post_data = array(
			'post_author'    => $from->ID,
			'post_status'    => 'publish',
			'post_title'     => $post_title,
			'post_name'      => md5( $post_title ),
			'post_type'      => self::$settings['post_type'],
			'post_content'   => $message
		);
		$message_id = wp_insert_post( $post_data );

		// Set up post meta
		$post_meta = array(
			'to'         => $to->ID,
			'subject'    => $subject,
			'attachment' => ($attachment ? esc_url($attachment) : 0),
			'reply'      => 0,
			'comment_id' => $comment_id,
		);

		// If this is a reply
		if ( is_numeric($reply) )
		{
			// Add the replied to id as post parent
			$post_data = array(
				'ID' => $message_id,
				'post_parent' => $reply,
			);
			wp_update_post( $post_data );
		}

		// Mark as private
		if ( $private ) do_action( self::PREFIX . '/mark_message/private', $message_id );

		// Mark as read
		if ( $read ) do_action( self::PREFIX . '/mark_message/read', $message_id );

		// Create the post meta
		foreach ( $post_meta as $meta_key => $meta_value )
		{
			#update_post_meta( $message_id, $meta_key, $meta_value );
			update_field( $meta_key, $meta_value, $message_id );
		}
/**/
		// If the unsubscribe plugin exists, check it before sending emails
		$no_messages_via_email = false;
		if ( class_exists('customize_subscription') )
		{
			$status = get_user_meta( $to->ID, 'customize_subscription/subscriptions', true );
			if ( in_array('messages', $status) ) $no_messages_via_email = true;
		}
/**/
		// Email the message also?
		if ( $email && ! $no_messages_via_email )
		{
			// Add the site name to the subject
			$subject .= ", " . get_option('blogname');

			// Make sure user has not requested not to get emails
			$no_messages_via_email = get_user_meta( $to->ID, 'no_messages_via_email', true );
			if (! $no_messages_via_email )
			{
				do_action( self::PREFIX . '/send_mail', array(
					'to'         => $to->user_email,
					'subject'    => $subject,
					'message'    => $message,
					'attachment' => $attachment,
					'from'       => $from,
				) );
			}
		}

		return $message_id;
	}

	/**
	 * Send message to user
	 *
	 * @author	Jake Snyder
	 * @since	1.0.0
	 * @param	array $args The arguments
	 *				$to			string	(required) email to send message to
	 *				$subject	string 	(required) The subject of the message, defaults: "New message from Sitename"
	 *				$message	string 	(required) The message to send
	 *				$attachment	string 	(optional) Path to file to attach to message, default: false
	 * @return	void
	 */
	public static function send_mail( $args )
	{
		$defaults = array(
			'to'         => false,
			'subject'    => 'New message',
			'message'    => false,
			'attachment' => false,
			'from'       => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$sender_email = apply_filters( self::PREFIX . '/from/email', get_bloginfo('admin_email') );
		add_filter( 'wp_mail_from', create_function('','return "' . $sender_email . '";') );

		$sender_name  = ( is_object($from) ) ? $from->display_name : apply_filters( self::PREFIX . '/from/name', get_bloginfo('blogname') );
		add_filter( 'wp_mail_from_name', create_function('','remove_filter( "bloginfo", "wptexturize" ); return "' . $sender_name . '";') );

		if ( $attachment && ! is_array($attachment) ) $attachment = array($attachment);

		$message = $subject . ":\n\n" . $message;

		$unsubscribe_url = apply_filters( 'customize_subscription/url/update_subscription', array('type' => 'messages') );
		if ( $unsubscribe_url ) $message .= "\n\nIf you don't want to receive these emails from " . get_option('blogname') . " in the future, please unsubscribe: " . $unsubscribe_url;

		@wp_mail($to, $subject, $message, NULL, $attachment);
	}

	/**
	 * Ajax wrapper for the add_message function
	 *
	 * @author	Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function ajax_add_message()
	{
		self::add_message( $_POST );
		echo json_encode( array('status' => 1) );
		die();
	}

	/**
	 * Get messages for a post or user
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $user_id (optional) The ID of the user to get messages for.
	 * @param	string $filter (optional) The a meta key to filter the messages by (read, unread, archived, new), default: false
	 * @return	void
	 */
	public static function get_messages( $args )
	{
		$defaults = array(
			'post_id' => false,
			'filter'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$messages = array();

		// get messages for post or user
		if ( is_numeric($post_id) )
		{
			$messages = apply_filters( self::PREFIX . '/get_post_messages', array('post_id' => $post_id, 'filter' => $filter) );
		}
		elseif ( 0 === strpos($post_id, 'user_') )
		{
			$user_id = str_replace('user_', '', $post_id);
			$messages = apply_filters( self::PREFIX . '/get_user_messages', array('user_id' => $user_id, 'filter' => $filter) );
		}

		return $messages;
	}

	/**
	 * Get messages for a post (used like comments), all, private, or public
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $post_id (optional) The ID of the post to get messages for.
	 * @param	string $filter (optional) The a meta key to filter the messages by (read, unread, archived, new), default: false
	 * @return	void
	 */
	public static function get_post_messages( $args )
	{
		$defaults = array(
			'post_id' => 0,
			'filter'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$user_id   = get_current_user_id();
		$author_id = get_post_field( 'post_author', $post_id );

		$messages = array();
		$filter_key = ( is_array($filter) ) ? implode('_', $filter) : $filter;
		if ( $filter_key && ! empty(self::$messages[$post_id][$filter_key]) )
			$messages = self::$messages[$post_id][$filter_key];
		elseif (! empty(self::$messages[$post_id]['all']) )
			$messages = self::$messages[$post_id]['all'];

		if (! $messages )
		{
			// Generally get all messages for a specific user
			$args = array(
				'post_type'      => self::$settings['post_type'],
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => -1,
				'post_parent'    => $post_id,
			);

			// Add filter arguments
			if ( $filter )
			{
				if (! is_array($filter) ) $filter = array($filter);

				// Private or public?
				if ( in_array('private', $filter) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => 'private',
					);
					// If the current user is not the author of the parent post, only show comments this user originated
					if ( $user_id != $author_id )
					{
						$args['post_author'] = $user_id;
					}
				}
				elseif ( in_array('public', $filter) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => array('private'),
						'operator' => 'NOT IN',
					);
				}
			}

			$messages = new WP_Query( $args );
			if ( $filter_key )
				self::$messages[$post_id][$filter_key] = $messages;
			else
				self::$messages[$post_id]['all'] = $messages;
			wp_reset_postdata();
		}

		return $messages;
	}

	/**
	 * Get messages for a user, all or different types
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $user_id (optional) The ID of the user to get messages for.
	 * @param	string $filter (optional) The a meta key to filter the messages by (read, unread, archived, new), default: false
	 * @return	void
	 */
	public static function get_user_messages( $args )
	{
		$defaults = array(
			'user_id' => false,
			'filter'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $user_id ) $user_id = get_current_user_id();

		$messages = array();
		if ( is_array($filter) ) sort($filter);
		$filter_key = ( is_array($filter) ) ? implode('_', $filter) : $filter;
		if ( $filter_key && ! empty(self::$messages[$user_id][$filter_key]) )
			$messages = self::$messages[$user_id][$filter_key];
		elseif (! empty(self::$messages[$user_id]['all']) )
			$messages = self::$messages[$user_id]['all'];

		if (! $messages )
		{
			// Generally get all messages for a specific user
			$args = array(
				'post_type'      => self::$settings['post_type'],
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => -1,
				'tax_query'      => array(),
				'meta_query'     => array(
					array(
						'key'     => 'to',
						'value'   => $user_id,
					),
				),
			);

			// Add filter arguments
			if ( $filter )
			{
				if (! is_array($filter) ) $filter = array($filter);

				if ( in_array('sent', $filter) )
				{
					if ( false !== ($key = array_search('sent', $filter)) )
					{
						unset($filter[$key]);
						unset($args['meta_query'][0]);
						$args['post_author'] = $user_id;
					}
				}

				$args['meta_query']['relation'] = 'AND';

				$tax_filters = $meta_filters = array();
				$tax_terms = array('unread','new','read','archived','archive','unarchived','unarchive','private','public');
				foreach ( $filter as $term )
				{
					if ( in_array(strtolower($term), $tax_terms) )
					{
						$meta_filters[] = $term;
					}
					else
					{
						$tax_filters[]  = $term;
					}
				}
				$meta_filters = array_map('strtolower', $meta_filters);

				// Read or unread?
				if ( in_array('unread', $meta_filters) || in_array('new', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => array('read'),
						'operator' => 'NOT IN',
					);
				}
				elseif ( in_array('read', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => 'read',
					);
				}

				// Archived or new?
				if ( in_array('archived', $meta_filters) || in_array('archive', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => 'archive',
					);
				}
				elseif ( in_array('unarchived', $meta_filters) || in_array('unarchive', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => array('archive'),
						'operator' => 'NOT IN',
					);
				}

				// Private or public?
				if ( in_array('private', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => 'private',
					);
				}
				elseif ( in_array('public', $meta_filters) )
				{
					$args['tax_query'][] = array(
						'taxonomy' => self::$settings['taxonomy'],
						'field'    => 'slug',
						'terms'    => array('private'),
						'operator' => 'NOT IN',
					);
				}

				if ( $tax_filters )
				{
					foreach ( $tax_filters as $filter )
					{
						$args['meta_query'][] = array(
							'key' => 'message_filters',
							'value' => $filter,
							'compare' => 'LIKE',
						);
					}
				}
			}

			$messages = new WP_Query( $args );
/** /
echo '<pre style="font-size:0.7em;text-align:left;">';
print_r($messages);
echo "</pre>\n";
#exit;
/**/
			if ( $filter_key )
				self::$messages[$user_id][$filter_key] = $messages;
			else
				self::$messages[$user_id]['all'] = $messages;
			wp_reset_postdata();
		}

/** /
		if (! empty($messages->post) && -1 == $messages->post->ID )
		{
			$messages->post = $messages->posts = array();
		}
/**/
		return $messages;
	}

	/**
	 * Get the permalink of a message without parent slug
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $post_id The ID of the post to create url for.
	 * @return	void
	 */
	public static function get_permalink( $post_id=false )
	{
		if (! $post_id && isset($GLOBALS['post']) ) $post_id = $GLOBALS['post']->ID;
		$post_type = self::$settings['post_type'];
		$post_name = get_post_field( 'post_name', $post_id );
		return home_url( "/$post_type/$post_name/" );
	}

	/**
	 * Output the permalink of a message without parent slug
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $post_id The ID of the post to create url for.
	 * @return	void
	 */
	public static function permalink( $post_id )
	{
		echo apply_filters( self::PREFIX . '/get_permalink', $post_id );
	}

	/**
	 * Indicator for new messages
	 *
	 * If new messages, shows how many. If no new messages, does nothing.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $user_id The ID of the user to get indicator for.
	 * @return	void
	 */
	public static function new_message_indicator( $args )
	{
		$defaults = array(
			'user_id' => false,
			'filter'  => false,
			'show_0'  => true,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$filters = array( 'unread' );
		if ( is_array($filter) ) $filters = array_merge( $filters, $filter );
		elseif  ( $filter ) $filters[] = $filter;

		$post_id = 'user_' . $user_id;

		$total_messages = apply_filters( self::PREFIX . '/total_messages', array('post_id' => $post_id, 'filter' => $filters) );

		if ( $show_0 || 0 < $total_messages )
		{
			echo '<span class="' . self::PREFIX . '_indicator badge">' . $total_messages . '</span>';
		}
	}

	/**
	 * Get unread messages total
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int $user_id The ID of the user to get messages for.
	 * @return	void
	 */
	public static function total_messages( $args )
	{
		$defaults = array(
			'post_id' => false,
			'filter'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$messages = apply_filters( self::PREFIX . '/get_messages', array('post_id' => $post_id, 'filter' => $filter) );

		return $messages->found_posts;
	}

	/**
	 * Add support to the frontend notificaitons plugin for our action query variables
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function add_notifications( $queries )
	{
		$queries = array_merge($queries, self::$settings['messages']);
		return $queries;
	}

	/**
	 * Install plugin options
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function install()
	{
		if (! get_option( self::PREFIX . '/installed' ) )
		{
			/**
			 * Add default status terms
			 */
			$taxonomy = 'message_status';
			$terms    = array(
				'read'    => 'Read',
				'archive' => 'Archived',
				'private' => 'Private',
			);
			foreach ( $terms as $key => $term )
			{
				if (! term_exists( $key, $taxonomy ) )
				{
					wp_insert_term(
						$term,
						$taxonomy,
						array( 'slug' => $key )
					);
				}
			}

			update_option( self::PREFIX . '/installed', current_time('timestamp') );
		}
	}

	/**
	 * Add support for the add_message form
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function form_head()
	{
		acf_form_head();
		do_action( self::PREFIX . '/enqueue_scripts' );
	}

	/**
	 * Load scripts and styles
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function enqueue_scripts()
	{
		/* removing colorbox and select2 for myswing * /
		wp_register_style( 'colorbox', self::$settings['dir'] . 'js/libs/colorbox/colorbox.css', array(), '', 'all' );
		wp_register_style( 'select2', self::$settings['dir'] . 'js/libs/select2/select2.css', array(), '', 'all' );
		wp_register_style( self::PREFIX, self::$settings['dir'] . 'css/style.css', array( 'colorbox', 'select2' ), '', 'all' );
		/** /
		wp_register_style( self::PREFIX, self::$settings['dir'] . 'css/style.css', array(), '', 'all' );
		/** /

		wp_enqueue_style( self::PREFIX );

		/* removing colorbox and select2 for myswing * /
		wp_register_script( 'colorbox', self::$settings['dir'] . 'js/libs/jquery.colorbox-min.js', '', '', true );
		wp_register_script( 'select2', self::$settings['dir'] . 'js/libs/select2/select2.min.js', '', '', true );
		wp_register_script( self::PREFIX . '_scripts', self::$settings['dir'] . 'js/scripts.js', array( 'jquery' ), '', true );
		wp_register_script( self::PREFIX, self::$settings['dir'] . 'js/messenger.js', array( 'jquery', 'colorbox', 'select2' ), '', true );
		/**/
		#wp_register_script( self::PREFIX . '_scripts', self::$settings['dir'] . 'js/scripts.js', array( 'jquery' ), '', true );
		wp_register_script( self::PREFIX, self::$settings['dir'] . 'js/messenger.js', array( 'jquery' ), '', true );
		/**/

		// Add variables for script
		$args = array(
			'nonce'		 => wp_create_nonce( 'lelearning-ajax-nonce' ),
			'spinner'	 => admin_url( 'images/spinner.gif' ),
			'url'		 => admin_url( 'admin-ajax.php' ),
			'prefix'     => self::PREFIX,
			'actions'    => array(
				'add'          => self::PREFIX . '_add_message',
				'mark'         => self::PREFIX . '_mark_message',
			),
			'view_selector' => apply_filters( self::PREFIX . '/template/view_selector', '#content article' ),
		);
		wp_localize_script( self::PREFIX, 'messenger', $args );
		wp_enqueue_script( array(
			self::PREFIX,
		) );
	}

	/**
	 * Output a basic list of messages
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	array $messages Messages to show
	 * @return	void
	 */
	public static function message_list( $messages, $type='user' )
	{
		if ( is_object($messages) && $messages->have_posts() ) : ?>
			<ul class="messages">
			<?php $count=0; while ( $messages->have_posts() ) : $messages->the_post(); ?>
				<?php do_action( self::PREFIX . '/message/' . $type ); ?>
			<?php endwhile;
			wp_reset_postdata(); ?>
			</ul>
		<?php else : ?>
			<p class="no-messages"><?php _e("No messages.", 'acf_messenger'); ?></p>
		<?php endif;
	}

	/**
	 * Load template files for the plugin.
	 *
	 * Tries to load from theme first to allow customization in the theme.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	string $filename Name of the template file to load
	 * @return	void
	 */
	public static function get_template( $filename )
	{
		$new_template = locate_template( array( $filename ) );
		if ( $new_template )
		{
			return $new_template;
		}
		return self::$settings['path'] . 'templates/' . $filename;
	}

	/**
	 * Set up message for inbox
	 *
	 * This builds a single message in a list of messages in the user's inbox.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function message_user()
	{
		$template = apply_filters( self::PREFIX . '/get_template', 'message-user.php' );
		include( $template );
	}

	/**
	 * Create a comment message to add to a post
	 *
	 * This builds a single message in a list of messages below a post that is being commented on.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function message_post()
	{
		$template = apply_filters( self::PREFIX . '/get_template', 'message-post.php' );
#echo ' $template = '. $template ."<br>\n";
		if ( $template ) include( $template );
	}

	/**
	 * Output a basic inbox list of messages
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	int $user_id (optional) The id of the user to get comments for, default: current user
	 * @return	void
	 */
	public static function inbox( $args )
	{
		$defaults = array(
			'user_id'                  => false,
			'show_compose_button'      => true,
			'show_view_archive_button' => true,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$filter = 'unarchived';
		if (! empty($_REQUEST['filter']) )
		{
			if ( false !== strpos($_REQUEST['filter'], ',') )
			{
				$filter = explode(',', $_REQUEST['filter']);
			}
			else
			{
				$filter = $_REQUEST['filter'];
			}
		}

		if (! $user_id ) $user_id = get_current_user_id();
		$post_id  = 'user_' . $user_id;
		$messages = apply_filters( self::PREFIX . '/get_messages', array('post_id' => $post_id, 'filter' => $filter) );
		?>
		<div class="<?php echo self::PREFIX; ?>_inbox">

			<?php if ( $show_compose_button ) do_action( self::PREFIX . '/compose_button', array( 'return_url' => 'inbox', 'class' => 'btn-primary' ) ); ?>

			<?php if ( $show_view_archive_button ) do_action( self::PREFIX . '/view_archive_button' ); ?>

			<?php do_action( self::PREFIX . '/message_list', $messages, 'user' ); ?>

		</div>
		<?php
	}

	/**
	 * Post comments
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	int $post_id (optional) The id of the post to get comments for, default: current, global $post->ID
	 * @return	void
	 */
	public static function comments( $args=false )
	{
		$defaults = array(
			'post_id'                  => false,
			'show_compose_button'      => true,
			'show_view_archive_button' => true,
		);
		$args = wp_parse_args( $args, $defaults );
		if (! $args['post_id'] ) $args['post_id'] = $GLOBALS['post']->ID;
		?>
		<div class="<?php echo self::PREFIX; ?>_comments">

			<?php if ( apply_filters( self::PREFIX . '/use_frontend_notifications', true ) ) do_action( 'frontend_notifications/show' ); ?>

			<?php do_action( self::PREFIX . '/comments/public', $args ); ?>

			<?php do_action( self::PREFIX . '/comments/private', $args ); ?>

		</div>
		<?php
	}

	/**
	 * Post public comments
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	int $post_id (optional) The id of the post to get comments for, default: current, global $post->ID
	 * @return	void
	 */
	public static function comments_public( $args=false )
	{
		$defaults = array(
			'post_id'                  => false,
			'show_compose_button'      => true,
			'show_view_archive_button' => true,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $post_id ) $post_id = $GLOBALS['post']->ID;

		$messages            = array();
		$messages['public']  = apply_filters( self::PREFIX . '/get_messages', array('post_id' => $post_id, 'filter' => 'public') );
		?>
		<div class="<?php echo self::PREFIX; ?>_public">
			<h2><?php _e( "Public Messages", 'acf_messenger' ); ?></h2>

			<?php do_action( self::PREFIX . '/compose_button', array(
				'reply_id'    => $post_id,
				'button_text' => __( "add Public Message", 'acf_messenger' ),
				'private'     => false,
			) ); ?>

			<?php do_action( self::PREFIX . '/message_list', $messages['public'], 'post' ); ?>
		</div>
		<?php
	}

	/**
	 * Post private comments
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @params	int $post_id (optional) The id of the post to get comments for, default: current, global $post->ID
	 * @return	void
	 */
	public static function comments_private( $args=false )
	{
		$defaults = array(
			'post_id'                  => false,
			'show_compose_button'      => true,
			'show_view_archive_button' => true,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $post_id ) $post_id = $GLOBALS['post']->ID;

		$messages            = array();
		$messages['private'] = apply_filters( self::PREFIX . '/get_messages', array('post_id' => $post_id, 'filter' => 'private') );
		?>
		<div class="<?php echo self::PREFIX; ?>_private">
			<h2><?php _e( "Private Messages", 'acf_messenger' ); ?></h2>

			<?php do_action( self::PREFIX . '/compose_button', array(
				'reply_id'    => $post_id,
				'button_text' => __( "add Private Message", 'acf_messenger' ),
				'private'     => true,
			) ); ?>

			<?php do_action( self::PREFIX . '/message_list', $messages['private'], 'post' ); ?>
		</div>
		<?php
	}

	/**
	 * Compose message form
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	int|string	$post_id ID of post to add message to or "user_{ID}" of user to send to.
	 * @param	bool		$private whether or not this message is private. Only matters for messages attached to posts.
	 * @return	void
	 */
	public static function compose_form( $args )
	{
		$defaults = array(
			'reply_id'    => false,
			'public'      => false,
			'return_url'  => false,
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if ( isset($_GET['public']) ) $public = $_GET['public'];

		if (! $reply_id   && ! empty($_GET['reply_id']) )   $reply_id   = $_GET['reply_id'];
		if (! $return_url && ! empty($_GET['return_url']) ) $return_url = urldecode($_GET['return_url']);
		if (! $return_url ) $return_url = get_permalink($reply_id);

		$field_groups = apply_filters( self::PREFIX . '/field_groups', array('acf_messenger_subject', 'acf_messenger_content') );
		if (! $reply_id ) $field_groups[] = 'acf_messenger_to'; ?>

		<form id="post" class="<?php echo self::PREFIX; ?>_form" action="" method="post">
			<div style="display:none">
				<input type="hidden" name="reply" value="<?php echo $reply_id; ?>" />
				<input type="hidden" name="public" value="<?php echo $public; ?>" />
			</div>
			<?php
			// ACF Form
			acf_form( array(
				'post_id'      => 'new_message',
				'form'         => false,
				'field_groups' => $field_groups,
				'return'       => add_query_arg( 'action', 'message-sent', $return_url ),
				#'submit_value' => __( "Send", 'acf_messenger' ),
			) ); ?> 
			<div class="field">
				<?php /**/ ?><input class="button btn btn-default <?php echo self::PREFIX; ?>-message-send" type="submit" value="<?php echo apply_filters( self::PREFIX . '/button/send', __( "Send", 'acf_messenger' ) ); ?>" /><?php /**/ ?>
				<?php /** / ?><a class="<?php echo self::PREFIX; ?>_send-message button btn btn-default button-send-message" href="#submit" title="<?php _e( "Send", 'acf_messenger' ); ?>"><?php _e( "Send", 'acf_messenger' ); ?></a><?php /**/ ?>
			</div>
		</form>
		<?php
	}

	/**
	 * Adds a link to load the compose form
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function compose_button( $args )
	{
		$defaults = array(
			'reply_id'    => false,
			'button_text' => false,
			'public'      => false,
			'return_url'  => get_permalink(),
			'class'       => '',
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		unset($args['button_text'], $args['class']);
		if ( 'inbox' == $args['return_url'] ) $args['return_url'] = apply_filters( self::PREFIX . '/inbox_url', '' );
		$args['return_url'] = urlencode( esc_url($args['return_url']) );

		if (! $button_text ) $button_text = __( "Send message", 'acf_messenger' );
		?>
		<a class="message-compose button btn btn-default btn-xs <?php echo $class .' '. self::PREFIX; ?>-message-compose" href="<?php echo add_query_arg( $args, home_url('/message-compose/') ); ?>" title="Send a message"><?php echo $button_text; ?></a>
		<?php
	}

	/**
	 * Adds a link to archive a given message
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function archive_button( $args )
	{
		$defaults = array(
			'post_id'     => false,
			'button_text' => false,
			'return_url'  => false,
			'submit_url'  => apply_filters( self::PREFIX . '/inbox_url', '' ),
			'class'       => '',
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $return_url ) $return_url = $submit_url;

		$action     = (! has_term('archive', 'message_status', $post_id) ) ? 'archive' : 'unarchive';
		$submit_url = add_query_arg( array('action' => $action, 'id' => $post_id, 'nonce' => wp_create_nonce($action), 'return_url' => urlencode(esc_url($return_url))), $submit_url );

		if (! $button_text || ! is_array($button_text) ) $button_text = array( __( "Archive message", 'acf_messenger' ), __( "Move to inbox", 'acf_messenger' ) );
		$button_text = ( 'archive' == $action ) ? $button_text[0] : $button_text[1];
		?>
		<a class="message-archive button btn btn-default btn-xs <?php echo $class .' '. self::PREFIX; ?>-message-archive" href="<?php echo $submit_url; ?>" title="<?php echo $button_text .': '. get_the_title($post_id); ?>"><?php echo $button_text; ?></a>
		<?php
	}

	/**
	 * Adds a link to archive a given message
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function read_button( $args )
	{
		$defaults = array(
			'post_id'     => false,
			'button_text' => false,
			'return_url'  => false,
			'submit_url'  => apply_filters( self::PREFIX . '/inbox_url', '' ),
			'class'       => '',
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if (! $return_url ) $return_url = $submit_url;

		$action     = (! has_term('read', 'message_status', $post_id) ) ? 'read' : 'new';
		$submit_url = add_query_arg( array('action' => $action, 'id' => $post_id, 'nonce' => wp_create_nonce($action), 'return_url' => urlencode(esc_url($return_url))), $submit_url );

		if (! $button_text || ! is_array($button_text) ) $button_text = array( __( "Mark as read", 'acf_messenger' ), __( "Mark as new", 'acf_messenger' ) );
		$button_text = ( 'read' == $action ) ? $button_text[0] : $button_text[1];
		?>
		<a class="message-read button btn btn-default btn-xs <?php echo $class .' '. self::PREFIX; ?>-message-read" href="<?php echo $submit_url; ?>" title="<?php echo $button_text .': '. get_the_title($post_id); ?>"><?php echo $button_text; ?></a>
		<?php
	}

	/**
	 * Adds a link to archive a given message
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function view_archive_button( $args )
	{
		$defaults = array(
			'button_text' => false,
			'submit_url'  => apply_filters( self::PREFIX . '/inbox_url', '' ),
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$new_filters = '';
		$current_filters = (! empty($_REQUEST['filter']) ) ? $_REQUEST['filter'] : '';

		if (! $button_text || ! is_array($button_text) ) $button_text = array( __( "View archive", 'acf_messenger' ), __( "View inbox", 'acf_messenger' ) );

		// Currently viewing archive
		if ( false !== strpos($current_filters, 'archive') )
		{
			$button_text = $button_text[1];
			$new_filters = str_replace(array('archive,','archive'), '', $current_filters);
		}
		// Currently viewing inbox
		else
		{
			$button_text = $button_text[0];
			$new_filters = 'archive,' . $current_filters;
		}

		if ( $new_filters ) $submit_url = add_query_arg( 'filter', rtrim($new_filters, ','), $submit_url );
		?>
		<a class="message-view-archive button btn btn-default btn-xs <?php echo $class .' '. self::PREFIX; ?>-message-view-archive" href="<?php echo $submit_url; ?>" title="<?php echo $button_text; ?>"><?php echo $button_text; ?></a>
		<?php
	}

	/**
	 * Add the custom post types used by application
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function add_post_types()
	{
		register_post_type( self::$settings['post_type'],
			array(
				'labels'              => array(
					'name'               => __( "Messages", 'acf_messenger' ),
					'singular_name'      => __( "Message", 'acf_messenger' ),
					'all_items'          => __( "All Messages", 'acf_messenger' ),
					'add_new'            => __( "Add New", 'acf_messenger' ),
					'add_new_item'       => __( "Add New Message", 'acf_messenger' ),
					'edit'               => __( "Edit", 'acf_messenger'  ),
					'edit_item'          => __( "Edit Messages", 'acf_messenger' ),
					'new_item'           => __( "New Message", 'acf_messenger' ),
					'view_item'          => __( "View Message", 'acf_messenger' ),
					'search_items'       => __( "Search Messages", 'acf_messenger' ), 
					'not_found'          => __( "Nothing found in the Database.", 'acf_messenger' ), 
					'not_found_in_trash' => __( "Nothing found in Trash", 'acf_messenger' ),
					'parent_item_colon'  => '',
				),
				'description'         => __( "Messages between site members.", 'acf_messenger' ),
				'public'              => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'show_ui'             => true,
				'query_var'           => true,
				'rewrite'	=> array( 'slug' => 'message', 'with_front' => false ),
				'has_archive' => 'messages', /* you can rename the slug here */
				'capability_type' => 'post',
				'hierarchical' => true,
				'supports' => array( 'title', 'editor', 'author', 'page-attributes' ),
			)
		);

			// add custom categories to courses
			register_taxonomy( self::$settings['taxonomy'], 
				array( self::$settings['post_type'] ),
				array(
					'hierarchical'      => true,
					'labels'            => array(
						'name'              => __( "Statuses", 'acf_messenger' ), /* name of the custom taxonomy */
						'singular_name'     => __( "Status", 'acf_messenger' ), /* single taxonomy name */
						'search_items'      => __( "Search Statuses", 'acf_messenger' ), /* search title for taxomony */
						'all_items'         => __( "All Statuses", 'acf_messenger' ), /* all title for taxonomies */
						'parent_item'       => __( "Parent Status", 'acf_messenger' ), /* parent title for taxonomy */
						'parent_item_colon' => __( "Parent Status:", 'acf_messenger' ), /* parent taxonomy title */
						'edit_item'         => __( "Edit Status", 'acf_messenger' ), /* edit custom taxonomy title */
						'update_item'       => __( "Update Status", 'acf_messenger' ), /* update title for taxonomy */
						'add_new_item'      => __( "Add New Status", 'acf_messenger' ), /* add new title for taxonomy */
						'new_item_name'     => __( "New Status Name", 'acf_messenger' ), /* name title for taxonomy */
					),
					'show_admin_column' => true,
					'show_ui'           => true,
					'query_var'         => true,
				)
			);
	}

	/**
	 * Register a field group for the messenger form
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function register_field_group()
	{
		if ( function_exists("register_field_group") )
		{
			register_field_group(array (
				'id' => 'acf_messenger_to',
				'title' => __("To", 'acf_messenger'),
				'fields' => array (
					array (
						'key' => 'field_53274ab7224fd',
						'label' => ( is_admin() ? '' : __("To", 'acf_messenger') ),
						'name' => 'to',
						'type' => 'user',
						'required' => ( is_admin() ? 0 : 1 ),
						'role' => apply_filters( self::PREFIX . '/to_role', array (
							0 => 'all',
						) ),
						'field_type' => 'select',
						'allow_null' => 1,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::$settings['post_type'],
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => ( is_admin() ? 'default' : 'no_box' ),
					'hide_on_screen' => array (
					),
				),
				'menu_order' => -20,
			));
			register_field_group(array (
				'id' => 'acf_messenger_subject',
				'title' => __("Subject", 'acf_messenger'),
				'fields' => array (
					array (
						'key' => 'field_53274ab7224fe',
						'label' => __("Subject", 'acf_messenger'),
						'name' => 'subject',
						'type' => 'text',
						'required' => ( is_admin() ? 0 : 1 ),
						'role' => apply_filters( self::PREFIX . '/to_role', array(
							0 => 'all',
						) ),
						'field_type' => 'select',
						'allow_null' => 1,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::$settings['post_type'],
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => ( is_admin() ? 'default' : 'no_box' ),
					'hide_on_screen' => array (
					),
				),
				'menu_order' => -15,
			));
			register_field_group(array (
				'id' => 'acf_messenger_filters',
				'title' => __("Filters", 'acf_messenger'),
				'fields' => array (
					array (
						'key' => 'field_53513ba26df73',
						'label' => __("Add Filters to Message", 'acf_messenger'),
						'name' => 'message_filters',
						'type' => 'select',
						'choices' => apply_filters( self::PREFIX . '/filters', array (
							'Friend' => 'Friend',
						) ),
						'default_value' => '',
						'allow_null' => 1,
						'multiple' => 1,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::$settings['post_type'],
							'order_no' => 0,
							'group_no' => 0,
						),
					),
					array (
						array (
							'param' => 'ef_user',
							'operator' => '==',
							'value' => 'all',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
			register_field_group(array (
				'id' => 'acf_messenger_add_filter',
				'title' => __("Add New Filter", 'acf_messenger'),
				'fields' => array (
					array (
						'key' => 'field_535113047ef67',
						'label' => __("Filter Name", 'acf_messenger'),
						'name' => 'new_filter',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'ef_user',
							'operator' => '!=',
							'value' => 'all',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
			register_field_group(array (
				'id' => 'acf_messenger_content',
				'title' => __("Message", 'acf_messenger'),
				'fields' => array (
					array (
						'key' => 'field_53274ab72345rcontent',
						'label' => __("Message", 'acf_messenger'),
						'name' => 'form_post_content',
						'type' => 'textarea',
						'required' => 1,
						'role' => apply_filters( self::PREFIX . '/to_role', array(
							0 => 'all',
						) ),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => '',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => -10,
			));
			/** /register_field_group(array (
				'id' => 'acf_messenger_attachment',
				'title' => 'Message attachment',
				'fields' => array (
					array (
						'key' => 'field_53274a92224fc',
						'label' => 'Attachment',
						'name' => 'attachment',
						'type' => 'file',
						'save_format' => 'url',
						'library' => 'uploadedTo',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::$settings['post_type'],
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 10,
			));/**/
		}

	}
}

endif;