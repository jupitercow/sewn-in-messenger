# Sewn In Messenger

A plugin to manage messages between users.

This plugin creates an on-site inbox for users that collects messages from other users. Messages can be sent directly or this can be used to create public and/or private message areas on posts similar to comments. Email notifications are also sent to users.

## Requirements

This plugin requires Advanced Custom Fields. It uses ACF to manage forms and uses its hoooks to create functionality.

## Optional

Works with our Frontend Notifications plugin, in fact, it is included in case you are not using it already. If you are, it will use that, if not, it will include it and add its own notification output area.

Works with Bootstrap Modal. We are using colorbox to show messages and forms, but if you have Bootstrap Modal, you can use the plugin with that.

Works with our Customize Subscription plugin to turn off emails for users.

## A message framework

The goal is to create a flexible framework for managing messages. There are a number of actions and filters for adding functionality directly into your templates. There are also javascript hooks for customizing messages through ajax.

