<?php
do_action( 'acf_messenger/form_head' );
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<header class="article-header">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
					</header>

					<section class="entry-content clearfix" itemprop="articleBody">
						<?php do_action( 'acf_messenger/compose_form' ); ?>
					</section>

				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>