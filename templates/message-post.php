<?php global $post; ?>
<li class="message <?php echo (! has_term('read', 'message_status', $post) ) ? 'unread' : 'read'; ?>" data-id="<?php the_ID(); ?>">
	<div class="avatar">
		<?php echo get_avatar( $post->post_author, 100 ); ?>
		

	</div>
	<div class="meta">
		<?php _e( "From", 'acf_messenger' ); ?> <a href="<?php echo get_author_posts_url($post->post_author); ?>"><?php the_author_meta( 'display_name', $post->post_author ); ?></a> | <?php echo get_the_date(); ?>
	</div>
	<div class="content">
		<?php echo apply_filters( 'the_content', $post->post_content ); ?>
	</div>
</li>