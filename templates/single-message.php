<?php
#do_action( 'acf_messenger/form_head' );
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<header class="article-header">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
 						<p class="byline vcard entry-meta">
							<?php printf( __('Sent <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>', 'acf_messenger'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), '<a href="' . get_author_posts_url($post->post_author) . '">' . get_the_author_meta( 'display_name', $post->post_author ) . '</a>'); ?>
							&rsaquo; <?php do_action( 'acf_messenger/compose_button', array(
								'reply_id'    => get_the_ID()
							) ); ?>
							<?php do_action( 'acf_messenger/archive_button', array(
								'post_id'     => get_the_ID()
							) ); ?>
							<?php do_action( 'acf_messenger/read_button', array(
								'post_id'     => get_the_ID()
							) ); ?>
						</p>

						<?php
						$parent = get_post_field( 'post_parent', get_the_ID() );
						if ( $parent ) : ?>
							<p class="post">
								<?php $post_type = get_post_type( $parent ); ?>
								<?php if ( 'message' != $post_type ) : ?>
										Related to: <?php echo get_the_title( $parent ); ?>
								<?php /** /else : ?>
										Reply to: <?php get_the_title( $parent );/**/ ?>
								<?php endif; ?>
							</p>
						<?php endif; ?>
					</header>

					<section class="entry-content clearfix" itemprop="articleBody">
						<?php the_content(); ?>
					</section>

				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>