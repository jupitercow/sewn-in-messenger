<?php global $post; ?>
<li class="message <?php echo (! has_term('read', 'message_status') ) ? 'unread' : 'read'; ?>" data-id="<?php the_ID(); ?>">
	<span class="status">
		<?php if (! has_term('read','message_status') ) echo '<span class="new-indicator">' . __( "New", 'acf_messenger' ) . '</span>'; ?> 
	</span>
	<span class="date">
		<?php printf( __( 'Date: %s', 'acf_messenger' ), get_the_date() ); ?> 
	</span>
	<span class="from">
		<?php printf( __( 'From: %s', 'acf_messenger' ), '<a href="' . get_author_posts_url($post->post_author) . '">' . get_the_author_meta( 'display_name', $post->post_author ) . '</a>' ); ?> 
	</span>
	<span class="classification">
		<?php if ( has_term('private', 'message_status') ) :
			$term = get_term_by( 'slug', 'private', 'message_status' );
			echo $term->name;
		endif; ?> 
	</span>
	<a class="read-message" href="<?php do_action( 'acf_messenger/permalink' ); ?>" data-nonce="<?php echo wp_create_nonce('read'); ?>">
		<?php _e( "View", 'acf_messenger' ); ?> 
	</a>
</li>